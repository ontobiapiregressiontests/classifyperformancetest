# -*- coding: utf-8 -*-
import unittest
import requests
import json
import xmlrunner
import csv
import datetime
import sys

class InequalityTest(unittest.TestCase):

  urlp2 = ""
    
  def Ontobi_API(self, S1):
        url = 'https://api.ontobi.com/' + self.urlp2 + '/fid'
        payload = {"msgId":"hello","bodyS1":S1}
        headers = {'content-type': 'application/json'}
        session = requests.Session()
        r = session.post(url, data = json.dumps(payload), headers = headers)
        j = r.json()
        output = j["data"]["categories"][0]["id"]
        return output


  def test_IsClassify_PerformanceTest(self):
   print("Loading....")
   ClassifyDoc=open('classifyUnitTests.csv')
   GivenInputs=csv.DictReader(ClassifyDoc)
   BODYS1=[]
   CATID=[]
   for row in GivenInputs:
    CATID.append(row['categoryId'])
    BODYS1.append(row['bodyS1'])
   ClassifyDoc.close()
   lenInputRows=len(CATID)

   #StartTime
   StartTime = datetime.datetime.now()

   for i in range(1,lenInputRows):
    expected = CATID[i]
    bodys1=BODYS1[i]
    Actuval=self.Ontobi_API(bodys1)
    print(Actuval)

   #EndTime
   EndTime = datetime.datetime.now()
   #TakenTime
   TimeTaken=EndTime-StartTime
   #InSeconds
   TakenSeconds=int(TimeTaken.total_seconds())
   self.assertTrue(TakenSeconds<3)


if __name__ == '__main__':
    InequalityTest.urlp2 = sys.argv[1]
    suite = unittest.TestLoader().loadTestsFromTestCase(InequalityTest)
    xmlrunner.XMLTestRunner(output='test-reports').run(suite)